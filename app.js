const express = require('express');
const cors = require('cors');
const errorHandler = require('./middleware/error');
const cookieParser = require('cookie-parser');

// const apiRouter = require('./api');
// const errorHandler = require('./helpers/errorHandler');
// process.env.JWT_PRIVATE_KEY = "secret2";
// process.env['JWT_EXPIRE'] = "30d";

const { register, verifyAccount } = require('./contollers/registration');
const { login, logout, refreshToken } = require('./contollers/auth');
const { forgotPassword, resetPassword } = require('./contollers/accounts');
const server = express();
let corsOptions = { methods: ['POST', 'GET'] };
if (process.env.ENV == 'prod') {
  corsOptions = {
    credentials: true,
    origin: 'https://momentvm.co.uk',
  };
}
if (process.env.ENV == 'dev') {
  corsOptions = {
    credentials: true,
    origin: 'http://localhost.com',
  };
}

server.use(cors(corsOptions));
server.use(cookieParser());
server.use(express.urlencoded({ extended: true, strict: false }));
server.use(express.json());
server.get('/', (req, res) => {
  res.json({ message: 'Express API Powered by AWS Lambda!' });
});
server.post('/login', login);
server.get('/logout', logout);

server.get('/refreshtoken', refreshToken);

server.get('/register/verify', verifyAccount);
server.post('/register', register);

server.post('/accounts/forgot-passsword', forgotPassword);
server.post('/accounts/reset-password', resetPassword);

server.get('/test', (req, res) => {
  res.json({ message: 'test' + process.env.AUTH_PRIVATE_KEY });
});

server.use(errorHandler);
module.exports = server;
