const sns = require('../aws/sns');
module.exports = {
  async sendActivationEmail(email, userId, email_verification_token) {
    return new Promise (async (resolve,reject)=>{
      const topic = 'momentum-email-verification';
      try {
        if(process.env.ENV!='prod'){
          resolve('sent')
          return
        }
        await sns.publish(topic, {
          recepient: email,
          userId: userId,
          token: email_verification_token,
        });
        resolve('sent')
      } catch (error) {
        reject('failed')
      }
    })
  },
  async sendForgotPasswordEmail(email, userId, resetPasswordToken) {
    return new Promise (async (resolve,reject)=>{
      const topic = 'momentum-forgot-password';
      try {
        if(process.env.ENV!='prod'){
          resolve('sent')
          return
        }
        await sns.publish(topic, {
          recipient: email,
          userId,
          resetPasswordToken
        });
        resolve('sent')
      } catch (error) {
        reject('failed')
      }
    })
  }
};
