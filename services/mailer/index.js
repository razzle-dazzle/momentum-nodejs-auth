// const sns = require('../aws/sns');
const SNSMailer = require('./SNSMailer');
const EmailTypes = require('./entities/emailTypes')
class Mailer {
  static async sendActivationEmail(
    user = { email: String, id: String, email_verification_token: String }
  ) {
    const { email, id, email_verification_token } = user;
    return SNSMailer.sendActivationEmail(email, id, email_verification_token);
  }

  static async send(EMAIL_TYPE, data = { email: String, id: String }) {
    if(EMAIL_TYPE==EmailTypes.FORGOT_PASSWORD_EMAIL){
      const { email, userId, resetPasswordToken } = data;
      return SNSMailer.sendForgotPasswordEmail(email, userId, resetPasswordToken);
    }
  }
}

module.exports = { Mailer };
