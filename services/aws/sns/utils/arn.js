
module.exports = {
  makeTopicArn(topic,options = { region:'eu-west-2', accountOwnerId: process.env.AWS_ACCOUNT_ID } ){
    return `arn:aws:sns:${options.region}:${options.accountOwnerId}:${topic}`
  }
}