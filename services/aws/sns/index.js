const { SNS } = require('aws-sdk');
const { makeTopicArn } = require('./utils/arn');

module.exports = {
  async publish(topic, data) {
    return new Promise(async (resolve, reject) => {
      const sns = new SNS({ region: 'eu-west-2' });
      const params = {
        Message: JSON.stringify(data),
        TopicArn: makeTopicArn(topic),
      };
      await sns.publish(params).promise().then((data)=>{
        resolve('sent')
      }).catch((err)=>{
        reject('failed')
      })
    });
  },
};
