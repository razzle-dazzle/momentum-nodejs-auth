const SES = require('aws-sdk').SES;
exports.sendTemplateEmail = async function (
  recepients,
  templateName,
  templateData,
  options = {
    region: 'eu-west-2',
    sender: {
      identity,
      sender: "noreply"
    }
  }
) {
  return new Promise((resolve, reject) => {
    const ses = new SES();
    var params = {};
    if (options.region) ses.config.update({ region: options.region });
    if (options.sender.identity){
      params.Source = `Momentvm Team<${options.sender.sender}@${options.sender.identity}>`
      params.SourceArn = `arn:aws:ses:${options.region}:${process.env.AWS_ACCOUNT_ID}:identity/${options.sender.identity}`
    }
    Object.assign(params,{
      Destination: {
        ToAddresses: recepients
      },
      Template: templateName,
      TemplateData: JSON.stringify(templateData),
    })

    ses.sendTemplatedEmail(params, function (err, data) {
      if (err) reject(false)
      else resolve(true);
    });
  });
};
