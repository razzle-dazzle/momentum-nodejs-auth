const asyncHandler = require('../middleware/async');
const ErrorResponse = require('../utils/errorResponse');
const { User } = require('../models/User');
const jwt = require('jsonwebtoken')

exports.login = asyncHandler(async (req, res, next) => {
  const { email, password } = req.body;

  // Validate email and password
  if (!email || !password)
    return next(new ErrorResponse(`Please provide an email and password`, 400));

  // Check for user
  const user = await User.getUserByEmail(email);

  if (!user) return next(new ErrorResponse(`Invalid credentials`, 401));

  // Check if password matches
  const isMatch = await user.matchPassword(password);

  if (!user.get('email_verified'))
    return next(new ErrorResponse(`Verify your email first`, 400));

  if (!isMatch) return next(new ErrorResponse(`Invalid credentials`, 401));

  // // Create token
  // Create token
  sendTokenResponse(user, 200, res);
});

exports.refreshToken = asyncHandler(async (req, res, next) => {
  const { refresh_token } = req.cookies;

  // Validate email and password
  if (!refresh_token || refresh_token == "undefined")
    return next(new ErrorResponse(`No refresh token provided`, 400));

  try {
    // Verify token
    const decoded = jwt.verify(refresh_token, process.env.JWT_PUBLIC_KEY, algorithm="RS256");

    user = await User.getUserById(decoded.id);

    // let refresh_token = user.getSignedJwtToken(expiry);
    // Reset refresh token

    sendTokenResponse(user, 200, res);
  } catch (err) {
    return next(
      new ErrorResponse('Not authorized to access this resource', 401)
    );
  }
});

// Get token from model, create cookie and send response
const sendTokenResponse = (user, statusCode, res) => {
  // Create token
  const token_expiry = new Date(Date.now() + 15 * 60 * 1000);
  const token = user.getSignedJwtToken('15m');
  const refresh_token_expiry = new Date(Date.now() + 1000 * 60 * 60 * 24 * 7);
  const refresh_token = user.getSignedJwtToken('7d');

  res
    .status(statusCode)
    .cookie('token', token, makeOptions(token_expiry,process.env.CSFR_TOKEN_DOMAIN))
    .cookie('refresh_token', refresh_token, makeOptions(refresh_token_expiry,process.env.CSFR_REFRESH_DOMAIN))
    .json({
      success: true,
      token,
      refresh_token,
    });
};

exports.logout = asyncHandler(async (req, res, next) => {
  const token_expiry = new Date(Date.now() + 30 * 1000);
  let options = makeOptions(token_expiry,process.env.CSFR_TOKEN_DOMAIN)
  res
    .cookie('token', 'none', options)
    .cookie('refresh_token', 'none', options);

  res.status(200).json({
    success: true,
    data: {},
  });
});

function makeOptions (expiry,domain=''){
  let options = {
    expires: expiry,
    path: '/',
    sameSite: 'Lax',
    httpOnly: true
  }

  if (process.env.ENV == 'prod') {
    options.secure = true;
  }

  if(domain)
    options.domain = domain

  return options;
};