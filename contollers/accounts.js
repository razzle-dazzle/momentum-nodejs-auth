const asyncHandler = require('../middleware/async');
const ErrorResponse = require('../utils/errorResponse');
const { User } = require('../models/User');

exports.resetPassword = asyncHandler(async (req, res, next) => {
  const { password, token, userId } = req.body;

  if (!token || !password|| !userId)
    return next(new ErrorResponse(`Please provide the password ,userId and token`, 400));

  let user = await User.getUserById(userId);
  if (!user) return next(new ErrorResponse(`Could not find user`, 400));

  user
    .resetPassword(password,token)
    .then(() => {
      res.status(204).send();
    })
    .catch((err) => {
      return next(new ErrorResponse(err, 500));
    });

  // if (user) {
  //   res.status(204).send();
  // } else return next(new ErrorResponse(`Could not update password`, 500));
});

exports.forgotPassword = asyncHandler(async (req, res, next) => {
  const { email } = req.body;

  if (!email)
    return next(new ErrorResponse(`Please provide an email address`, 400));

  let user = await User.getUserByEmail(email);

  if (!user) {
    res.status(202).send();
    return;
  }

  let emailVerified = user.get('email_verified');
  if (!emailVerified) {
    res.status(202).send();
    return;
  }

  user
    .forgotPassword()
    .then(() => {
      res.status(202).send();
    })
    .catch((error) => {
      next(new ErrorResponse(`Something went wrong`, 500));
    });
});
