const asyncHandler = require('../middleware/async');
const ErrorResponse = require('../utils/errorResponse');
const { User } = require('../models/User');

exports.register = asyncHandler(async (req, res, next) => {
  const { email, password } = req.body;

  if (!email || !password)
    return next(new ErrorResponse(`Please provide email and password`, 400));

  let user = await User.getUserByEmail(email);
  if (user) return next(new ErrorResponse(`Already registered`, 400));

  user = await User.create(email, password);

  if (user) {
    res.status(204).send();
  }
  else 
  return next(new ErrorResponse(`Could not create user`, 500));
});

exports.verifyAccount = asyncHandler(async (req, res, next) => {
  const { token , userId } = req.query;

  if (!token || !userId )
    return next(new ErrorResponse(`Please provide token and userId`, 400));

  let user = await User.getUserById(userId);
  if (!user) return next(new ErrorResponse(`Token or userId not valid`, 400));

  let emailVerified= user.get('email_verified')
  if (emailVerified)
    return next(new ErrorResponse(`Already verified`, 400));
  
  let emailVerificationToken = user.get('email_verification_token');

  if(emailVerificationToken != token)
    return next(new ErrorResponse(`Token or userId not valid`, 400));

  user.set({email_verified: true, email_verification_token: null})
  
  user.update(
    function(err){
      if(err)
        next(new ErrorResponse(`Could not verify email`, 500));
      res.status(204).send();
    });  
});

