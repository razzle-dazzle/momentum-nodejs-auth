var dynamo = require('dynamodb');
var Joi = require('joi');
const crypto = require('crypto');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const { Mailer } = require('../services/mailer');
const {
  FORGOT_PASSWORD_EMAIL,
} = require('../services/mailer/entities/emailTypes');
var { v4: uuidv4 } = require('uuid');
const { throws } = require('assert');

const params = {
  // region: 'eu-west-2',
  // endpoint: 'http://localhost:3369',
};
if (process.env.DYNAMODB_REGION) params.region = process.env.DYNAMODB_REGION;
if (process.env.DYNAMODB_ENDPOINT != '') {
  params.endpoint = process.env.DYNAMODB_ENDPOINT;
}
dynamo.AWS.config.update(params);

var UserModel = dynamo.define('User', {
  hashKey: 'pk',
  rangeKey: 'sk',

  timestamps: true,
  createdAt: 'created_at',
  updatedAt: 'updated_at',

  schema: {
    pk: Joi.string().required(),
    id: Joi.string().required(),
    sk: Joi.string().default('CREDENTIALS#'),
    email: Joi.string().required().email(),
    password: Joi.string().required(),
    password_reset_token: Joi.string().default(null),
    email_verification_token: Joi.string().default(uuidv4()),
    email_verification_token_expiry: Joi.string().default(xDaysFromNow(1)),
    email_verified: Joi.boolean().default('false'), //TODO: change to false and implement SNS SES
    auth_refresh_token: Joi.string(),
    auth_token: Joi.string(),
  },
  indexes: [
    {
      hashKey: 'email',
      rangeKey: 'sk',
      name: 'GSI_email-sk',
      type: 'global',
    },
  ],
  tableName: process.env.TABLE_NAME || 'momentum',
});

function xDaysFromNow(x = 0) {
  var date = new Date();
  date.setDate(date.getDate() + x);
  return date.toUTCString();
}
class User {
  constructor(record) {
    this.record = record;
  }
  get(args) {
    return this.record.get(args);
  }
  set(args) {
    this.record.set(args);
  }
  async update(args) {
    return this.record.update(args);
  }
  async matchPassword(enteredPassword) {
    return await bcrypt.compare(enteredPassword, this.record.get('password'));
  }
  getSignedJwtToken = function (expiresIn) {
    return jwt.sign(
      {
        id: this.get('id'),
        email: this.get('email'),
      },
      process.env.JWT_PRIVATE_KEY,
      { algorithm: 'RS256', expiresIn: expiresIn }
    );
  };

  /**
   * @returns {Promise<boolean>} Successful?
   */
  async forgotPassword() {
    return new Promise(async (resolve, reject) => {
      let resetPasswordToken = uuidv4();
      this.set({ password_reset_token: resetPasswordToken })
      this.update()
        .then(async (model) => {
          await Mailer.send(FORGOT_PASSWORD_EMAIL, {
            resetPasswordToken,
            userId: this.record.get('id'),
            email: this.record.get('email'),
          })
            .then(() => {
              resolve(true);
            })
            .catch((err) => {
              reject(false);
            });
        })
        .catch((err) => {
          reject(false);
        });
    });
  }

  /**
   * 
   * @param {String} password
   * @param {String} token
   * @returns {Promise<boolean>}
   */
  async resetPassword(password,token) {
    return new Promise(async (resolve, reject) => {
      let savedToken = this.get('password_reset_token')
      if(!savedToken || savedToken!=token)
        reject("Tokens do not match")
      let newPassword = await hashPassword(password);
      this.set({ password: newPassword, password_reset_token: null })
      this.update()
        .then(async (model) => {
          resolve(true)
        })
        .catch((err) => {
          reject("Could not update password");
        });
    });
  }

  static async create(email, password) {
    return new Promise(async (resolve, reject) => {
      try {
        let hashedPassword = await hashPassword(password);
        let id = uuidv4();
        let pk = `USER#${id}`;
        await UserModel.create(
          {
            pk,
            id,
            email,
            password: hashedPassword,
          },
          async function (err, usr) {
            if (err) {
              resolve(false);
            }
            await Mailer.sendActivationEmail(usr.attrs);
            resolve(usr);
          }
        );
      } catch (err) {
        reject(false);
      }
    });
  }
  /**
   *
   * @param {String} email
   * @returns {Promise<User>} User Service Instance
   */
  static async getUserByEmail(email) {
    return new Promise((resolve, reject) => {
      UserModel.query(email)
        .usingIndex('GSI_email-sk')
        .where('sk')
        .equals('CREDENTIALS#')
        .exec(function (err, data) {
          if (err) {
            reject(false);
          }
          if (data && data.Items && data.Items.length > 0)
            resolve(new User(data.Items[0]));
          resolve(false);
        });
    });
  }
  /**
   * 
   * @param {String} userId 
   * @returns {Promise<User>}
   */
  static async getUserById(userId) {
    return new Promise((resolve, reject) => {
      UserModel.get(
        {
          pk: `USER#${userId}`,
          sk: 'CREDENTIALS#',
        },
        function (err, user) {
          if (err) reject(false);
          if (user) {
            resolve(new User(user));
          }
          resolve(false);
        }
      );
    });
  }
}

async function hashPassword(password) {
  const salt = await bcrypt.genSalt(10);
  const hashedPassword = await bcrypt.hash(password, salt);
  return hashedPassword;
}

module.exports = {
  User,
};
